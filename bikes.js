/* global $, google */

var xmlhttp = new XMLHttpRequest();
var url = "https://api.citybik.es/v2/networks/citybikes-helsinki";
var bikeStations;
var map;
var markers = [];

$(function () {
    
    function initMap() {
        var location = new google.maps.LatLng(60.1839, 24.9002);

        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: location,
            zoom: 12,
        };
        map = new google.maps.Map(mapCanvas, mapOptions);
    }
    
    google.maps.event.addDomListener(window, 'load', initMap);
    
});

xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var bikeArr = JSON.parse(xmlhttp.responseText);
            bikeStations = bikeArr.network.stations;
            populateStations(bikeStations);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

function populateStations(bikeStations) {
        var out = "";
        for(var i = 0; i < bikeStations.length; i++) {
            var location = {lat: bikeStations[i].latitude, lng: bikeStations[i].longitude};
            out += "<div class='panel panel-default'>" +
                        "<div class='panel-heading'>" +
                            "<h4 class='panel-title'>" +
                                "<a data-toggle='collapse' data-parent='#accordion' href='#collapse"+i+"' onclick='createMarker(" + location.lat + "," + location.lng + ")'>" + bikeStations[i].name + "</a>" +
                            "</h4>" +
                        "</div>" +
                        "<div id='collapse"+i+"' class='panel-collapse collapse'>" +
                            "<div class='panel-body'>Free bikes: " + bikeStations[i].free_bikes + " and empty slots: " + bikeStations[i].empty_slots + "</div>" +
                        "</div>" +
                    "</div>";
        }
        document.getElementById("accordion").innerHTML = out;
    }
      
function createMarker(lat, lng) {
        removeMarkers();
        updateWeather(lat, lng);
        
        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map
        });
        markers.push(marker);
}

function updateWeather(lat, lng){
    var wurl = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon="+ lng +"&appid=a8bef477e3c820e50e9dc96ed9fb716d";
    var weather;
    var main;
    
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var weatherArr = JSON.parse(xmlhttp.responseText);
            weather = weatherArr.weather;
            main = weatherArr.main;
            populateWeather(weather, main);
        }
    };
    xmlhttp.open("GET", wurl, true);
    xmlhttp.send();
    
    function populateWeather(weather, main){
        var temp = (main.temp - 273.15).toFixed(1);
        var pic = "http://openweathermap.org/img/w/" + weather[0].icon + ".png";
        var out = temp + '<img src="' + pic + '" alt="Weather icon" >';
        
        document.getElementById("weather").innerHTML += out;
    }
    
}



function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
}

function removeMarkers() {
    setMapOnAll(null);
}